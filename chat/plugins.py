from flask_migrate import Migrate
from chat.metrics import Metrics

migrate = Migrate()
metrics = Metrics()