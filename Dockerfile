# Development dockerfile
FROM python:2.7

ENV PYTHONUNBUFFERED 1
ENV FLASK_APP=chat.app
ENV FLASK_ENV=development

COPY requirements.txt /tmp/requirements.txt
RUN pip install \
        --no-compile \
        --no-cache-dir \
        --requirement /tmp/requirements.txt && \
    mkdir /code


WORKDIR /code
COPY . /code

CMD flask run --host 0.0.0.0 --port 5003
